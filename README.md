# dab
## naming
**dab** is the
- **d**umb **a**verage **b**uilder (when it works and you realize it's the same as make but worse)
- **d**umb **a**ss **b**uilder (when it fails and you realize you're using bad software)
- **d**evoted **a**mazing **b**ash masterpiece (when you ascend and this software becomes the Holy Grail)  
  
also dab is relevant amirite :^)
## installation
```
git clone https://github.com/InitializeSahib/dab.git
sudo ./dab/dab -f dab -t install
# manually:
# sudo mv dab/dab /usr/bin
# sudo chmod +x /usr/bin/dab
```
  
## usage

your project must have a `Dabfile`  
a basic one looks like the following
```
build() {
    compile src/main.c main
}
default() {
    build
}
```  
  
this will compile `src/main.c` and link it as `main` when either `dab` or `dab -t build` is run in the Dabfile's directory  
the `default` directive is optional but without it `dab` will not work (without arguments it looks for `default`)  
`compile` is a built-in directive that basically runs `cc src/main.c -o main` (for more fine-tuning use `cc` instead of `compile`)  
any valid shell commands (`ls`, `cp`, `rm`, etc.) are valid  
  
## complaints / suggestions
there's an [issue tracker](https://github.com/InitializeSahib/dab/issues) here for a reason  
(although please don't expect me to maintain this, this was made purely for ironic enjoyment)  
(don't use this in your project)  
(please)  
